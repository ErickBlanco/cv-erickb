import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Header from '../../components/header/header.component';
import AboutMe from '../../components/about-me/about-me.component';
import ContactInfo from '../../components/contact-info/contact-info.component';
import EventList from '../../components/event-list/event-list.component';
import RankList from '../../components/rank-list/rank-list.component';

import { 
    selectName,
    selectTitle,
    selectAbout,
    selectContactInfo,
    selectLanguages,
    selectEducationAsEvents,
    selectProfessionalExperienceAsEvents,
    selectOthersAsEvents,
    selectSkills
} from '../../redux/cv/cv.selectors';

import './home.styles.scss';

const HomePage = ({
    name,
    title,
    about,
    contactInfo,
    languages,
    skills,
    education,
    professionalExperience,
    others,
}) => (
    <div className='homepage'>
        <Header className='header' name={name} title={title} />

        <div className='body'>
            <div className='side-nav-bar'>
                <ContactInfo {...contactInfo} />
                <hr />
                <RankList title='Languages' items={languages} />
                <hr />
                <RankList title='Skills' items={skills}/>
            </div>
            <div className='content'>
                <AboutMe content={about} />
                <EventList title='EDUCATION' items={education} />
                <EventList title='WORK EXPERIENCE' items={professionalExperience} />
                <EventList title='OTHERS' items={others} />
            </div>
        </div>
    </div>
);

const mapStateToProps = createStructuredSelector({
    name: selectName,
    title: selectTitle,
    about: selectAbout,
    contactInfo: selectContactInfo,
    languages: selectLanguages,
    education: selectEducationAsEvents,
    professionalExperience: selectProfessionalExperienceAsEvents,
    others: selectOthersAsEvents,
    skills: selectSkills
});

export default connect(mapStateToProps)(HomePage);
