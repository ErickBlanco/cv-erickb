import CV_DATA from './cv.data';

const INITIAL_STATE = {
    data: CV_DATA
};

const cvReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default cvReducer;