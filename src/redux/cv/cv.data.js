const CV_DATA = {
    name: 'Erick J. Blanco',
    title: 'Computer Science Student',
    aboutMe: `
        Computer Science student, with experience mainly in
        web (front end) and mobile development. Proficient in
        many programming languages and technologies. Competitive
        Programming enthusiast.
    `,
    contactInfo: {
        city: 'Chihuahua, México',
        phone: '(614) 140 0239',
        email: 'erick.jassi.bs@gmail.com',
        linkedin: 'linkedin.com/in/erickjbs/'
    },
    languages: [
        {
            name: 'Spanish',
            proficiency: 'Native',
            stars: 5
        },
        {
            name: 'English',
            proficiency: 'Professional Working',
            stars: 3
        }
    ],
    skills: [
        'Proactive and self-taught',
        'Problem solving skills',
        'Object-oriented analysis',
        'Commitment to quality assurance',
        'Self-management skills'
    ],
    professionalExperience: [
        {
            role: 'Web and Mobile Developer',
            roleDescription: `
                Development tasks in frontend (Angular),
                backend (Firebase, Node.js),
                Database (Firestore, MongoDB)
                and mobile applications (Flitter). 
            `,
            company: 'Leading Technologies',
            location: 'Chihuahua, MX',
            startingDate: '02/19',
            endDate: 'Present',
        }
    ],
    education: [
        {
            name: 'Computer Science Engineering',
            school: 'UACH Faculty of Engineering',
            location: 'Chihuahua, MX',
            startYear: 2016,
            graduationYear: 2020
        },
        {
            name: 'Colegio de Bachilleres del Estado de Chihuahua',
            school: 'COBACH Pl. 1',
            location: 'Chihuahua, MX',
            startYear: 2013,
            graduationYear: 2016
        }
    ],
    other: [
        {
            name: 'Intensive Training Program (ITP)',
            description: `
                Member of the second generation of the
                training program provided by the company.
            `,
            location: 'Prokarma México',
            startingDate: '10/19',
            endDate: 'Present',
        }
    ]
};

export default CV_DATA;