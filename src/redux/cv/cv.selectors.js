import { createSelector } from 'reselect';

const selectCv = state => {
    console.log(state);
    return state.cv;
};

const selectCvData = createSelector(
    [selectCv],
    cv => cv.data
);

export const selectName = createSelector(
    [selectCvData],
    data => data.name
);

export const selectTitle = createSelector(
    [selectCvData],
    data => data.title
);

export const selectAbout = createSelector(
    [selectCvData],
    data => data.aboutMe
);

export const selectContactInfo = createSelector(
    [selectCvData],
    data => data.contactInfo
);

export const selectLanguages = createSelector(
    [selectCvData],
    data => data.languages.map(({name, proficiency, stars}) => {
        return {
            title: name,
            description: proficiency,
            rank: stars
        }
    })
);

export const selectSkills = createSelector(
    [selectCvData],
    data => data.skills.map((skill) => {
        return {
            title: skill
        }
    })
);

export const selectEducationAsEvents = createSelector(
    [selectCvData],
    data => data.education.map(item => {
        const { name, school, location, startYear, graduationYear } = item;
        return {
            title: name,
            description: school,
            location,
            start: startYear,
            end: graduationYear
        };
    })
);

export const selectProfessionalExperienceAsEvents = createSelector(
    [selectCvData],
    data => data.professionalExperience.map(item => {
        const { role, company, location, startingDate, endDate } = item;
        return {
            title: role,
            description: company,
            location,
            start: startingDate,
            end: endDate
        };
    })
);

export const selectOthersAsEvents = createSelector(
    [selectCvData],
    data => data.other.map(item => {
        const { name, description, location, startingDate, endDate } = item;
        return {
            title: name,
            description: description,
            location,
            start: startingDate,
            end: endDate
        };
    })
);