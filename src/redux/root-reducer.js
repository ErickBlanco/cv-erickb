import { combineReducers} from 'redux';

import cvReducer from './cv/cv.reducer';

const rootReducer = combineReducers({
    cv: cvReducer
});

export default rootReducer;