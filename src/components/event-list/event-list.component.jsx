import React from 'react';

import EventItem from './event-item/event-item.component';

import './event-list.styles.scss';

const EventList = ({ title, items = []}) => (
    <div className='event-list'>
        <h3 className='event-list-title'>{title}</h3>
        <div className='event-list-items'>
            {
                items.map((item, index) => (
                    <EventItem key={index}
                        {...item} />
                ))
            }
        </div>
    </div>
);

export default EventList;