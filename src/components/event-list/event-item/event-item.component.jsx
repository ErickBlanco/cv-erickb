import React from 'react';

import './event-item.styles.scss';

const EventItem = ({ title, description, location, start, end }) => (
    <div className='event-item'>
        <h5 className='event-title'>{`${title} (${start}-${end})`}</h5>
        <h5 className='event-subtitle'>{`${description}, ${location}`}</h5>
    </div>
);

export default EventItem;