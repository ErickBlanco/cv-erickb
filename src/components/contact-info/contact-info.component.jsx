import React from 'react';
import { faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { faPhone, faLocationArrow, faEnvelope } from '@fortawesome/free-solid-svg-icons';

import IconItem from '../icon-item/icon-item.component';

import './contact-info.styles.scss';

const ContactInfo = ({ city, phone, email, linkedin }) => (
    <div className='contact-info'>
        <IconItem icon={faLocationArrow} title={city} />
        <IconItem icon={faPhone} title={phone} />
        <IconItem icon={faEnvelope} title={email} />
        <IconItem link icon={faLinkedin} title={linkedin} />
    </div>
);

export default ContactInfo;