import React from 'react';

import './about-me.styles.scss';

const AboutMe = ({ content }) => (
    <div className='about-me'>
        <h3 className='title'>ABOUT ME</h3>
        <p>{content}</p>
    </div>
);

export default AboutMe;