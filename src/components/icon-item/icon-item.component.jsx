import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './icon-item.styles.scss';

const IconItem = ({ icon, title, link }) => (
    <div className='icon-item'>
        <FontAwesomeIcon icon={icon} />
        {
            link? 
                <a href={title} className='title'>{title}</a>
            :
                <p className='title'>{title}</p>
        }
    </div>
);

export default IconItem;