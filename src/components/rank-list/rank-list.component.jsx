import React from 'react';

import RankItem from './rank-item/rank-item.component';

import './rank-list.styles.scss';

const RankList = ({ title, items = [] }) => (
    <div className='rank-list'>
        <span className='title'>{title}</span>
        <div className='items'>
            {
                items.map((item, index) => (
                    <RankItem key={index}
                        {...item} />
                ))
            }
        </div>
    </div>
);

export default RankList;