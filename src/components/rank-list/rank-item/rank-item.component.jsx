import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';

import './rank-item.styles.scss';

const renderStars = (rank) => {
    const stars = [];
    for (let i=0; i<rank; i++) {
        stars.push(<FontAwesomeIcon key={i} icon={faStar} />);
    }
    return stars;
}

const RankItem = ({ title, description, rank }) => (
    <div className='rank-item'>
        <span>{title}</span>
        {
            description ? (
                <span>
                    <span> - </span>
                    <span className='description'>{description}</span>
                </span>
            ) : ''
        }
        <div className='star-container'>
            {
                renderStars(rank)
            }
        </div>
    </div>
);

export default RankItem;