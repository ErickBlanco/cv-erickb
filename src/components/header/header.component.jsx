import React from 'react';

import './header.styles.scss';

const Header = ({ name, title }) => (
    <div className='header'>
        <p className='name'>{ name }</p>
        <span className='title'>{ title }</span>
    </div>
);

export default Header;